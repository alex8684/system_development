/*
 * day_10.cpp
 *
 * Created: 01-04-2020 21:10:39
 * Author : Alexander With
 */ 

#define F_CPU 16000000
#define FOSC 16000000
#define BAUD 9600
#define MYUBRR FOSC/16/BAUD - 1

#include <avr/io.h>
#include <stdint.h>
#include <avr/interrupt.h>
#include <util/delay.h>

void processData(char data);
void uartSend(unsigned char data);

volatile char uartData; 
volatile bool uartFlag = false;

int main(void){
	cli();
	UBRR0H = ((MYUBRR) >> (8));
	UBRR0L = MYUBRR;
	
	UCSR0B |= (1 << RXEN0) | (1 << TXEN0);		// Enable receiver and transmitter
	UCSR0B |= (1 << RXCIE0);					// Enable receiver interrupt
	UCSR0C |= (1 << UCSZ01) | (1 << UCSZ00);	// Set frame: 8bit, 1stop
    sei();
	
	DDRC |= (1 << DDC2) | (1 << DDC1);
	while (1) {
		if(uartFlag){
			processData(uartData);
			uartFlag = false;
		}
//		else{
//			asm("NOP");
//		}
    }
}

void processData(char data){
	if((data == '3')){
		PORTC |= (1 << PINC2);
		uartSend('1');
		uartSend('\r');
	}
	else if((data == '4')){
		PORTC &= ~(1 << PINC2);
		uartSend('0');
		uartSend('\r');
	}
	else{
		uartSend('E');
		uartSend('\r');
	}
	uartData = 0;
}

void uartSend(unsigned char data){
	while(!(UCSR0A  & (1 << UDRE0)));
	UDR0 = data;
}

ISR(USART_RX_vect){
	PORTC ^= (1 << PINC1);
	uartFlag = true;
	uartData = UDR0;
}