/*
 * project.cpp
 *
 * Created: 16-04-2020 15:26:34
 * Author : Alexander With
 */ 
#define F_CPU 16000000
#define BAUD 76800
#define BAUDRATE F_CPU/16/BAUD -1

#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdlib.h>

char RXdata[8];
bool uartFlag = false;
volatile uint8_t RXidx = 0;

void uartSend(const char *data);
int changeDuty(const char *data);


int main(void){
    cli();
	UBRR0H = ((BAUDRATE) >> (8));				// Baud: 76800 - Upper Bits
	UBRR0L = BAUDRATE;							// Baud: 76800 - Lower Bits
	UCSR0B |= (1 << RXEN0) | (1 << TXEN0);		// Enable receiver and transmitter
	UCSR0B |= (1 << RXCIE0);					// Enable receiver interrupt
	UCSR0C |= (1 << UCSZ01) | (1 << UCSZ00);	// Set frame: 8bit, 1stop
	sei();
	
	OCR0A = 255;								// Duty Cycle: 100%
	TCCR0A |= (1 << COM0A1);					// None-inverted mode
	TCCR0A |= (1 << WGM00);						// PWM Phase Corrected - Top: FF
	TCCR0B |= (1 << CS01);						// Pre-scaler Clock / 8
	
	DDRD |= (1 << DDD7);
	DDRD |= (1 << DDD6);
	
	char local_buf[64];
	int local_rxidx = 0;
	int idx = 0;
	
    while (1){
		if (local_rxidx != RXidx) {
			char ch = RXdata[local_rxidx];
			local_buf[idx++] = ch;
			if(ch == '\r'){
				uartFlag = true;
				local_buf[idx] = 0;
			}
			local_rxidx++;
			local_rxidx &= 7;
		}
				
		if(uartFlag){
			int change = changeDuty(local_buf);
			const char* TXdata = (change ? "OK\r\n" : "ER\r\n");
			uartSend(TXdata);
			idx = 0;
			uartFlag = false;
		}
    }
}

void uartSend(const char *data){
	while (*data) {
		while(!(UCSR0A & (1 << UDRE0)));
		UDR0 = *data;
		data++;
	}
}

int changeDuty(const char *data){
	int dutyCycle = atoi(data);
	// data is a string
	if(dutyCycle == 0 && *data != '0'){
		return 0;
	}
	// data is out of range
	if(dutyCycle < 0 || dutyCycle > 255){
		return 0;
	}
	OCR0A = dutyCycle;
	return 1;
}

ISR(USART_RX_vect){
	RXdata[RXidx++] = UDR0;
	RXidx &= 7;
	PORTD ^= (1 << PIND7);
}