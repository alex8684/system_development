/*
 * setup.c
 *
 * Created: 14-02-2020 09:06:32
 *  Author: Alexander With
 */ 

#define TIMERDELAY 2

#include "setup.h"
#include <avr/io.h>
#include <avr/interrupt.h>

void setup(void){
	// LED pin (PB0)
	DDRB = (1<<PINB0);
	PORTB = 0x00;
	
	// Button pin (PD2)
	DDRD = 0x00;
	PORTD = 0x00;
}

void interruptSetup(void){
	cli();	// Clear global interrupts
	
	// Set external interrupt0
	EICRA = ((1<<ISC01) | (1<<ISC00));	// Rising edge interrupt
	EIMSK = (1<<INT0);
	
	// Set internal timer1 - CTC mode 4 - 2 Sec
	TCCR1B = ((1<<WGM12) | (1<<CS12) | (1<<CS10));
	OCR1A = (F_CPU / 1024) * TIMERDELAY - 1;
	
	sei();	// Enable global interrupts
}