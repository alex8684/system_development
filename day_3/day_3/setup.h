/*
 * setup.h
 *
 * Created: 14-02-2020 09:09:21
 *  Author: Alexander With
 */ 


#ifndef SETUP_H_
#define SETUP_H_

void setup(void);
void interruptSetup(void);

#endif /* SETUP_H_ */