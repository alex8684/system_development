/*
 * interruptHandler.c
 *
 * Created: 14-02-2020 11:15:15
 *  Author: Alexander With
 */ 

#include <avr/io.h>
#include <avr/interrupt.h>

ISR(INT0_vect){
	TCNT1 = 0x0000;			// Reset timer1 counting register
	PORTB |= (1<<PINB0);	// Turn on PB0
	TIMSK1 |= (1<<OCIE1A);	// Start timer1
}

ISR(TIMER1_COMPA_vect){
	PORTB &= ~(1<<PINB0);	// Turn off PB0
	TIMSK1 &= ~(1<<OCIE1A);	// Stop timer1
}