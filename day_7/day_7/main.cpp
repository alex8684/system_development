/*
 * day_7.cpp
 *
 * Created: 16-04-2020 15:26:34
 * Author : Alexander With
 */ 
#define F_CPU 16000000
#define BAUD 38400
#define BAUDRATE F_CPU/16/BAUD -1

#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdlib.h>

bool uartFlag = false;
char RXdata[8];
volatile uint8_t RXidx = 0;		// Max size: 8

void uartSend(const char *data);
int changeDuty(const char *data);

int main(void){
	char data_buf[64];
	uint8_t local_RXidx = 0;	// Max size: 8
	uint8_t data_idx = 0;
	
    cli();
	UBRR0H = ((BAUDRATE) >> (8));				// Baud: 76800 - Upper Bits
	UBRR0L = BAUDRATE;							// Baud: 76800 - Lower Bits
	UCSR0B |= (1 << RXEN0) | (1 << TXEN0);		// Enable receiver and transmitter
	UCSR0B |= (1 << RXCIE0);					// Enable receiver interrupt
	UCSR0C |= (1 << UCSZ01) | (1 << UCSZ00);	// Set frame: 8bit, 1stop
	sei();
	
	OCR0A = 255;								// Duty Cycle: 100%
	TCCR0A |= (1 << COM0A1);					// None-inverted mode
	TCCR0A |= (1 << WGM00);						// PWM Phase Corrected - Top: FF
	TCCR0B |= (1 << CS01);						// Pre-scaler Clock / 8
	
	DDRD |= (1 << DDD6) | (1 << DDD7);
	
    while (1){
		if (local_RXidx != RXidx) {
			char ch = RXdata[local_RXidx];
			if(ch){
				data_buf[data_idx] = ch;
				data_idx++;
			}
			if(ch == '\r'){
				uartFlag = true;
				data_buf[data_idx] = 0;
			}
			local_RXidx++;
			local_RXidx &= 7;	// Buffer Mask
		}
				
		if(uartFlag){
			int change = changeDuty(data_buf);
			const char* TXdata = (change ? "OK\r\n" : "ER\r\n");
//			uartSend(data_buf);
			uartSend(TXdata);
			data_idx = 0;
			uartFlag = false;
		}
    }
}

void uartSend(const char *data){
	while (*data) {
		while(!(UCSR0A & (1 << UDRE0)));
		UDR0 = *data;
		data++;
	}
}

int changeDuty(const char *data){
	char *end;
	int dutyCycle = strtoul(data, &end, 10);
	// data is a string
	if(data == end){
		return 0;
	}
	// data is out of range
	if(dutyCycle < 0 || dutyCycle > 255){
		return 0;
	}
	OCR0A = dutyCycle;
	return 1;
}

ISR(USART_RX_vect){
	RXdata[RXidx++] = UDR0;
	RXidx &= 7;		// Buffer Mask
	// 00001000
	// 00000111
	PORTD ^= (1 << PIND7);
}