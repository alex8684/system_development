#include <avr/io.h>
#include <stdint.h>
#include <stdlib.h>
#include <avr/interrupt.h>

#define F_CPU 16000000
#define BAUD 9600
#define BitRate F_CPU/16/BAUD -1

volatile char ReceivedChar;

int main(void)
{
	/* set baud rate */
	UBRR0H = ((BitRate) >> (8));
	UBRR0L = BitRate;

	UCSR0B |= (1 << RXEN0) | (1 << TXEN0);        // enable receiver and transmitter
	UCSR0C |= (1 << UCSZ01) | (1 << UCSZ01);    // set frame: 8data, 1 stp

	sei();                                        // remember #include <avr/interrupt.h>

	DDRD |= (1 << DDD6);
	//    DDB3 is now an output

	OCR0A = 128;
	// set PWM for 50% duty cycle

	TCCR0A |= (1 << COM0A1);
	// set none-inverting mode

	TCCR0A |= (1 << WGM02) | (1 << WGM00);
	// set PWM phase corrected mode

	TCCR0B |= (1 << CS01);

	while (1)
	{

	}

}

ISR (USART_RX_vect)
{
	ReceivedChar = UDR0;                        // Read data from RX Buffer
	UDR0 = ReceivedChar;                        // Write the data to the TX Buffer
}

