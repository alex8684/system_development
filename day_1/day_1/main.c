/*
 * day_1.c
 *
 * Created: 31-01-2020 09:35:31
 * Author : Alexander With
 */ 

#define F_CPU 8000000 // 8MHz

#include <util/delay.h>
#include <avr/io.h>

#define FREQ 100

void setup(void);
void blink(int stop);

int main(void){
	setup();
    blink(10);
	return 0;
}

void setup(void){
	DDRB |= 1<<5; //0x20;	// 0b 0010 0000
	PORTB = 0x00;	// 0b 0000 0000
}

void blink(int stop){
	short int counter = 0;
	while(1){
		if((counter += 1) < stop*2){
//			PORTB ^= 0x20;	// 0b 0010 0000
			PORTB ^= 1<<5;	// 0b 0010 0000
			_delay_ms(FREQ);
		}
		else {
			break;
		}
	}
	PORTB = 0x00;	//0b 0000 0000
}
