/*
 * day_5.cpp
 *
 * Created: 13-03-2020 22:13:57
 * Author : With
 */ 

#define F_CPU 16000000

#include <avr/io.h>
#include <util/delay.h>

void setup(int duty_cycle);

int main(void){
	float value = 0;
	
	DDRD = (1 << PIND6);
	TCCR0A = (1 << COM0A1);
	TCCR0A |= (1 << WGM01) | (1 << WGM00);
	TCCR0B = (0 << CS02) | (0 << CS01) | (1 << CS00);

    while (1){
		OCR0A = (sinf(value)+1)*64+5;
		_delay_ms(10);
		value += 0.03;
		if(value >= M_PI*2){
			value -= M_PI*2;
		}
    }
}

void setup(int duyt_cycle){
	DDRD = (1 << PIND6);
	OCR0A = duyt_cycle;
	TCCR0A = (1 << COM0A1);
	TCCR0A |= (1 << WGM01) | (1 << WGM00);
	TCCR0B = (0 << CS02) | (1 << CS01) | (0 << CS00);
}