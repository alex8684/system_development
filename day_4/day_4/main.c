/*
 * day_4.c
 *
 * Created: 27-02-2020 08:56:51
 * Author : Alexander With
 */ 

#define F_CPU 8000000UL

#include <avr/io.h>
#include <avr/interrupt.h>
#include "setup.h"

int main(void){
	setup();
	setupInterrupt();
	
    while (1){
		if(TCNT1 >= 25000){
			PORTB ^= (1<<PINB5);
			TCNT1 = 0;
		}
    }
}