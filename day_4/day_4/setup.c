/*
 * setup.c
 *
 * Created: 27-02-2020 10:55:43
 *  Author: Alexander With
 */ 

#include <avr/io.h>
#include <avr/interrupt.h>

void setup(void){
	DDRB |= ((1<<PINB0) | (1<<PINB5));
	PORTB = 0x00;
}

void setupInterrupt(void){
	cli();
	// Timer1
	TCCR1B = ((1<<CS10) | (1<<CS11));
	sei();
}