/*
 * setup.h
 *
 * Created: 27-02-2020 11:08:56
 *  Author: Alexander With
 */ 


#ifndef SETUP_H_
#define SETUP_H_


void setup(void);
void setupInterrupt(void);


#endif /* SETUP_H_ */