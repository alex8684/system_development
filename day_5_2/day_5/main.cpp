/*
 * day_5.cpp
 *
 * Created: 08-03-2020 20:48:39
 * Author : Alexander With
 */ 

#include <avr/io.h>

void setup(void);

int main(void){
    setup();
    while (1) {
		
    }
}

void setup(void){
	DDRD = (1<<PIND6);
	OCR0A = 128;	// 50% duty cycle
	TCCR0A = (1<<COM0A1);	// non-inverted mode
	TCCR0A |= ((1<<WGM01) | (1<<WGM00));	// Fast PWM mode
	TCCR0B = (1<<CS01);	// Prescaler = 8
}